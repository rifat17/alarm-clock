import abc
from datetime import datetime, timedelta
import time
from threading import Thread, Lock
from dateutil import parser
import threading


# from datetime import datetime
# datetime.now().strftime("%H:%M")
# datetime.strptime("19:40", "%H:%M").strftime("%H:%M")

class IPublisher(abc.ABC):
    @abc.abstractmethod
    def addSubscriber(self, subscriber):
        raise NotImplementedError

    @abc.abstractmethod
    def removeSubscriber(self, subscriber):
        raise NotImplementedError

    @abc.abstractmethod
    def notifySubscriber(self):
        raise NotImplementedError


class ISubscriber(abc.ABC):

    @abc.abstractmethod
    def update(self, msg):
        raise NotImplementedError


class AlarmClock:
    def __init__(self, filepath, alarm_publisher: IPublisher):
        self.path = filepath
        self.alarms = []
        self.publisher = alarm_publisher

    def load_alarms(self):
        try:
            with open(self.path, 'r+') as file:
                for time in file:
                    loaded_time = datetime.strptime(time.strip(), "%Y-%m-%d %H:%M:%S")
                    alarm = Alarm(loaded_time)
                    self._register_alarm(alarm)

        except OSError as e:
            print(e)  # read the error to debug, TODO:  comment it later
            with open(self.path, 'w') as file:
                pass

    def set_alarm(self, alarm):
        # "%H:%M"
        with open(self.path, 'a') as file:
            file.write(f'{alarm.time}\n')
        self._register_alarm(alarm)

    def _register_alarm(self, alarm):
        self.alarms.append(alarm)
        self.publisher.addSubscriber(alarm)

    def get_alarms(self):
        return self.alarms

class Alarm(ISubscriber):
    cb = None

    def __init__(self, time):
        self._time = time
        self._run = True

    @property
    def run(self):
        return self._run

    @run.setter
    def run(self, value):
        self._run = False

    @property
    def time(self):
        return self._time

    @staticmethod
    def set_callback(fn):
        Alarm.cb = fn

    def update(self, msg):
        print(f'alarm {msg}\n')
        Alarm.cb()
        self.run = False

    def __str__(self):
        return f'Time={self.time}, Run={self.run}'

    def __repr__(self):
        return f'Time={self.time}, Run={self.run}'


class TimePublisher(IPublisher):

    def __init__(self):
        self.subscribers = set()  # set will contain instance of alarm class

    def addSubscriber(self, subscriber: ISubscriber):
        self.subscribers.add(subscriber)

    def removeSubscriber(self, subscriber: ISubscriber):
        self.subscribers.discard(subscriber)

    def notifySubscriber(self):
        # print(threading.current_thread().getName())
        # print(self.subscribers)
        now = datetime.now().strftime("%H:%M:%S")
        for subscriber in self.subscribers:
            
            # print(str(subscriber.time.time()), str(now))
            if str(subscriber.time.time()) == str(now):
                # print('EQUAL')
                if subscriber.run:
                    subscriber.update(now)

    def check_OS_time(self):
        # while True:
        #     now = datetime.now().strftime("%H:%M")  # 24h format, ex : 13:00
        pass
