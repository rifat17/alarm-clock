import threading
import time


from utils import (
    PATH_ALARM_TIME, PATH_YT_VIDEO,
    promot,play
)

from alarm import TimePublisher, AlarmClock, Alarm
publisher = TimePublisher()


def start_alarm(fn, lock):
    # global publisher 
    while True:
        with lock:
            fn()
            time.sleep(1)


publisher = TimePublisher()

clock = AlarmClock(PATH_ALARM_TIME, publisher)
clock.load_alarms()


Alarm.cb = play


lock = threading.Lock()

alarm_thread = threading.Thread(
    name='alarm',
    target=start_alarm,
    args=(publisher.notifySubscriber,lock),
    daemon=True
)
alarm_thread.start()

def main():
    global publisher
    promot()


main()


# app_thread = threading.Thread(name='app_thread', target=main, daemon=True)

# alarm_thread.start()
# app_thread.start()

# alarm_thread.join()
# app_thread.join()

