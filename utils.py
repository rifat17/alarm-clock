import datetime
import webbrowser
import re
import threading
import webbrowser
import random



PATH_ALARM_TIME = 'alarm_time.txt'
PATH_YT_VIDEO = 'yt_videos.txt'

VALID_YT_LINK_REGEX = "youtube\.com\/watch\?v=[a-zA-Z0-9\-_]+"




def play(link=None):
    b = webbrowser.get('chromium')

    links = get_from_file(PATH_YT_VIDEO)
    print(links)
    link = random.choice(links)
    print(link)

    b.open_new_tab(link)

def todayAt(hr, min, sec=0, micros=0):
    now = datetime.datetime.now()
    return now.replace(hour=hr, minute=min, second=sec, microsecond=micros)


def warn(msg: str):
    warntext = f'''\
####################################
    {msg}
####################################\
'''
    print(warntext)


def save_to_textfile(links: list, path):
    with open(path, 'a') as file:
        for link in links:
            file.write(f'{link}\n')


def get_from_file(path):
    data = []
    with open(path, 'r') as file:
        for line in file:
            data.append(line.strip())

    return data


def validate_yt_link(yt_link):
    link = re.findall(VALID_YT_LINK_REGEX, yt_link)
    if link:
        return link
    return None


def add_yt_link(path):
    run = True
    while run:
        response = None
        while response not in [1, 2, 3, 4]:
            print("\nOperations : ")

            try:
                response = int(input(
                    "\t[1] Add a youtube link\n\t[2] Add multiple yt-link\n\t[3] View saved links\n\t[4] Main menu\n"))
            except ValueError:
                warn('Please enter a numeric value.')

        if response == 1:
            given_link = input("Enter a youtube video link : ")

            link = validate_yt_link(given_link)
            if link:
                save_to_textfile(link, PATH_YT_VIDEO)
                msg = f'{link[0]} been saved.'  # indexing! not a good practice
                print(msg)
            else:
                warn(f"invalid link '{given_link}'")
        elif response == 2:
            quit_promoting = False
            links = []
            while quit_promoting is False:
                given_link = input("Enter a youtube video link (Enter q for Quit) : ")
                if given_link.lower() == 'q':
                    quit_promoting = True
                valid_link = validate_yt_link(given_link)
                if valid_link:
                    links.extend(valid_link)
                else:
                    warn(f"Invalid link '{given_link}'")
            if links:
                save_to_textfile(links, path)
                msg = f'Total {len(links)} video links been saved'
                print(msg)
                for i, link in enumerate(links, 1):
                    print(f'{i} : {link}')
        elif response == 3:
            loaded_links = get_from_file(PATH_YT_VIDEO)
            for i, link in enumerate(loaded_links, 1):
                print(f'{i} : {link.strip()}')
        elif response == 4:
            run = False


def alarms():
    run = True
    while run:
        response = None
        while response not in [1, 2, 3]:
            print("\nOperations : ")

            try:
                response = int(input("\t[1] Add alarm\n\t[2] view alarms\n\t[3] Main menu\n"))
            except ValueError:
                warn('Please enter a numeric value.')

        if response == 1:
            add_alarm()
        elif response == 2:
            view_alarms()
        elif response == 3:
            run = False


def add_alarm():
    alarm_time = input("Enter alarm time (in 24h format. ex : 13:20): ")

    try:
        h, m = [int(x) for x in alarm_time.split(':')]
        alarm_time = todayAt(h, m)
        save_to_textfile([alarm_time], PATH_ALARM_TIME)
    except:
        warn('Invalid time')


def view_alarms():
    path = PATH_ALARM_TIME
    loaded_alarms = get_from_file(path)
    for alarm in loaded_alarms:
        print(alarm.strip())


def promot():
    run = True
    while run:
        print(threading.current_thread().getName())
        response = None
        while response not in [1, 2, 3]:
            print("\nOperations : ")
            try:
                response = int(input("\t[1] Add youtube link\n\t[2] Alarms\n\t[3] Exit\n"))
            except ValueError:
                warn('Please enter a numeric value.')
        if response == 1:
            add_yt_link(PATH_YT_VIDEO)
        elif response == 2:
            alarms()
        elif response == 3:
            run = False # not working, maybe due to thread?

        print('=' * 30)







if __name__ == '__main__':
    # promot()
    play()
