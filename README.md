# BUGs

* different thread maintain different state
    * due to this bug, added alarm/yt-video link do not get updated in current session,
    but got saved newly added alarm time/yt-video links into different file in real time.

    to get this alarm and play the videos in setted/saved time, one have to exit the program,
    then start again, while starting, program reads all alarm/yt-links in main thread, and work as expected.
    

    * have to rewrite the whole 'utils.py' for multi threading support


## TODOs:
1. de-coupling
2. maintain naming convention
3. seperate functinlity into different file
4. ...
5. ...

```sh

git clone
cd alarm-clock
python app.py

```